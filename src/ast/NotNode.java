package ast;

import lib.FOOLlib;
import lib.TypeException;

public class NotNode implements Node{

	private Node exp;

	public NotNode(Node e) {
		this.exp = e;
	}

	@Override
	public String toPrint(String s) {
		return s+"Not\n"+exp.toPrint(s+"  ");
	}

	public Node typeCheck() throws TypeException {
		Node e = exp.typeCheck();  
		if ( !(FOOLlib.isSubtype(e, new BoolTypeNode()))) {
			System.out.println("Incompatible types in not");
			System.exit(0);	
		}  
		return new BoolTypeNode();
	}

	@Override
	public String codeGeneration() {
		String l1= FOOLlib.freshLabel();
		String l2= FOOLlib.freshLabel();
		String code = exp.codeGeneration();
		return code+
			   "push 0\n"+
			   "beq "+l1+"\n"+
			   "push 0\n"+
			   "b "+l2+"\n"+
			   l1+": \n"+
			   "push 1\n"+
			   l2+": \n";
	}
	
	/*
	 * Inserisco 0 nella pila, che corrisponde a false
	 * tramite "beq" confronto se lo 0 in pila è uguale all'input ricevuto
	 * se sono uguali salto a l1, il quale effettua il push dell'opposto di 0, cioè 1, cioè true.
	 * Altrimenti continuo con l'istruzione successiva, la quale effettua il push di 0, cioè false.
	 * Effettuo un salto incondizionato per poter avere soltanto l'ultimo elemento inserito nella pila.
	 * */
}