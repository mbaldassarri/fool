package ast;
import lib.*;
//2 cfu project
public class AndNode implements Node{
	
	private Node left;
	private Node right;
	
	public AndNode(Node left, Node right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String toPrint(String s) {
		return s+"And\n"+this.left.toPrint(s+"  ")+this.right.toPrint(s+"  ");
	}

	public Node typeCheck() throws TypeException{
		if ( ! ( FOOLlib.isSubtype(left.typeCheck(), new BoolTypeNode()) &&
				FOOLlib.isSubtype(right.typeCheck(), new BoolTypeNode()) ) ) {
			System.out.println("Non booleans in and");
			System.exit(0);	
		}
		return new BoolTypeNode();
	}
  
  public String codeGeneration() {
    String l1= FOOLlib.freshLabel(); //x
    String l2= FOOLlib.freshLabel(); //y
	  return left.codeGeneration()+
			 "push 0\n"+  
		     "beq "+l1+"\n"+
		     right.codeGeneration()+
		     "push 0\n"+  
		     "beq "+l1+"\n"+
		     "push 1\n"+ 
		     "b "+l2+"\n"+
		     l1+": \n"+ 
		     "push 0\n"+
		     l2+": \n";
	
	/* 
	 * NOTA:
	 * Mi basta un solo elemento false per far si che l'&& sia false.
	 * 
	 *  T T   T
 	 *  T F   F
     *  F T   F
     *  F F   F
     *  
	 * Inserisco nella pila il primo elemento da confrontare (left).
	 * Inserisco anche un valore arbitrario false sulla pila. 
	 * A questo punto la situazione è 
	 * 
	 * |F|
	 * |x|
	 * 
	 * Confronto i due elementi. se i due elementi sono uguali significa che x è false, quindi salto su l1 e restituisco direttamente false (perché (false && qualsiasi cosa) = false). 
	 * Se sono diversi significa che il primo elemento è true, quindi vado avanti: la pila è tornata vuota. 
	 * 
	 * Inserisco il secondo elemento y sulla pila e un valore arbitrario 0 (false). A questo punto la situazione è:
	 * 
	 * |F|
	 * |y|
	 * 
	 * Se i due elementi sulla pila sono uguali significa che y è false, quindi restituisco direttamente false (un valore qualsiasi && false = false).
	 * Se sono diversi significa che y è true quindi sono in una situazione in cui ho true sia per il primo (left) che per il secondo elemento (true), quindi restituisco true.
	 * (soltanto in questo caso restituisco true, quindi 1 sulla pila, in tutti gli altri casi 0 sulla pila).
	 * */
  }
  
}  