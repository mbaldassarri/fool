package ast;
import lib.*;
//2 cfu project
public class GreaterEqualNode implements Node {

  private Node left;
  private Node right;
  
  public GreaterEqualNode (Node l, Node r) {
   left=l;
   right=r;
  }
  
  public String toPrint(String s) {
   return s+"Equal\n" + left.toPrint(s+"  ")   
                      + right.toPrint(s+"  ") ; 
  }
    
  public Node typeCheck() throws TypeException {
	Node l= left.typeCheck();  
	Node r= right.typeCheck();  
    if ( !(FOOLlib.isSubtype(l, r) || FOOLlib.isSubtype(r, l)) ) 
    	throw new TypeException("Incompatible types in equal");
    return new BoolTypeNode();
  }
  
  public String codeGeneration() {
    String l1= FOOLlib.freshLabel();
    String l2= FOOLlib.freshLabel();
    String l3= FOOLlib.freshLabel();
	  return left.codeGeneration()+
			 right.codeGeneration()+
			 "bleq "+l1+"\n"+
			 "b "+l2+"\n"+
			 l1+": \n"+
			 left.codeGeneration()+
			 right.codeGeneration()+
			 "beq "+l2+"\n"+
			 "push 0\n"+
			 "b "+l3+"\n"+
			 l2+": \n"+
			 "push 1\n"+
			 l3+": \n";	         
  }
  
  /*
   * Devo confrontare due interi, quindi effettuo la codeGeneration di entrambi prima di iniziare (sia left che right). 
   * Questo inserisce i due numeri in pila.
   * Controllo se i due numeri in pila sono l'uno minore o uguale dell'altro tramite "bleq".
   * Se left NON è <= di right, significa che left è sicuramente > di right, quindi salto su l2, il quale ritorna true (push 1).
   * Se left è <= di right, salto su l1. Devo ritornare true nel caso siano uguali, altrimenti false. 
   * Quindi reinserendo i due interi in pila (codeGeneration), controllo esclusivamente se sono uguali.
   * Se lo sono, salto di nuovo su l2 effettuando il push di 1 sulla pila, cioè true, altrimenti effettuo il push di 0, cioè false.
   * Salto su l3 per terminare con successo l'operazione.
   * 
   * */
}  