package ast;
import lib.*;
//2 cfu project
public class LessEqualNode implements Node {

  private Node left;
  private Node right;
  
  public LessEqualNode (Node l, Node r) {
   left=l;
   right=r;
  }
  
  public String toPrint(String s) {
   return s+"Equal\n" + left.toPrint(s+"  ")   
                      + right.toPrint(s+"  ") ; 
  }
    
  public Node typeCheck() throws TypeException {
	Node l= left.typeCheck();  
	Node r= right.typeCheck();  
    if ( !(FOOLlib.isSubtype(l, r) || FOOLlib.isSubtype(r, l)) ) 
    	throw new TypeException("Incompatible types in equal");
    return new BoolTypeNode();
  }
  
  public String codeGeneration() {
    String l1= FOOLlib.freshLabel();
    String l2= FOOLlib.freshLabel();
	  return left.codeGeneration()+
			 right.codeGeneration()+
			 "bleq "+l1+"\n"+
			 "push 0\n"+
			 "b "+l2+"\n"+
			 l1+": \n"+
			 "push 1\n"+
			 l2+": \n";	         
  }
  
  /*
   * Inserisco i due interi sulla pila tramite il metodo codeGeneration
   * Effettuando il pop degli interi dalla pila, controllo tramite l'istruzione "bleq" se i 2 numeri sulla pila sono l'uno minore o uguale dell'altro.
   * Nel controllare i 2 elementi dalla pila vengono tolti.
   * Se lo sono, salto su l1, e restituisco true facendo push di 1 sulla pila.
   * Altrimenti effettuo il push di 0 (false) e salto su l2 per terminare.
   * */
}  